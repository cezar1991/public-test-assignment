<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>e-kierowca</title>
    <style>
        html, body {
            margin: 0;
            padding: 32px;

            background-color: #fff;
            color: #000;
            font-family: 'Arial', sans-serif;
        }
    </style>
</head>
<body>
<nav>
    <a href="{{ route('app') }}">
        Aplikacja
    </a>
</nav>
{{ $readme }}
</body>
</html>
