<p align="center">
    <img alt="e-kierowca logo" src="https://www.e-kierowca.pl/images/logo.png">
</p>

# Zadanie testowe

Chcielibyśmy zweryfikować Twoje umiejętności i podejście do wytwarzania kodu.
W tym celu przygotowaliśmy zadanie testowe wykorzystujący nasz stack technologiczny.

## Opis zadania

Należy stworzyć aplikację front-endową w React osadzoną na stronie serwowanej przez Laravel.
Aplikacja ma symulować działanie panelu administracyjnego i składać się z:
- lewne menu nawigacyjnego,
- górny pask menu
- głównego obszaru treści.

<table border="1" width="100%">
    <colgroup>
        <col width="25%"/>
        <col width="75%"/>
    </colgroup>
    <tr>
        <td colspan="2">
            menu
        </td>
    </tr>
    <tr height="200px">
        <td>
            nav
        </td>
        <td>
            content
        </td>
    </tr>
</table>

W menu nawigacyjnym mają znajdować się trzy elementy:
- "pulpit"
- "kurs"
- "użytkownicy"

Po kliknięciu w każdy z nich zmienia się komponent renderowany w obszarze głównym
(Wystarczy, że będzie to n.p.: `<h1>Puplit</h1>`) oraz adres w przeglądarce (strona ma się jednak nie odświeżać!).

Aplikacja ma się znajdować pod ścieżką `/app`. Ma ona byc dostępna dopiero po zalogowaniu. 
Do obsługi logowania wykorzystaj gotowe rozwiązania Laravela.

### Realizacja zadania
- [stwórz fork repozytorium](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/),
- utwórz gałąź assignment/imie-nazwisko,
- gdy będziesz gotowy wyślij pull-request przez bitbucket.

Projekt jest skonfigurowany do użycia React z `laravel-mix`,
wystarczy użyć polecenia `npm run prod` aby skompilować aplikację.
